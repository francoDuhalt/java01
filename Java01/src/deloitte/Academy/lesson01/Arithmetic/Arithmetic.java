package deloitte.Academy.lesson01.Arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.Academy.lesson01.run.Ejecutar;

public class Arithmetic {
	private static final Logger LOGGER = Logger.getLogger(Ejecutar.class.getName());
	
	public int x;
	public int y;
	public int r;
	/**
	 * @author Franco Duhalt
	 * @param Arithmetic Class
	 * @return resultados
	 */
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * 
	 * @param add
	 * @return
	 */
	public String addingNumber(Arithmetic add) {
		
		String result = "";
		
		try{
			r = x + y;
			result = "La suma de " + x + " + " + y + " es igual a: " + r;
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return result;
	}

	/**
	 * 
	 * @param subs
	 * @return
	 */
	public String subtracNumber(Arithmetic subs) {
		String result = "";
		
		try {
			r = x - y;
			result = "La resta de " + x + " - " + y + " es igual a: " + r;
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return result;
	}
	
	/**
	 * 
	 * @param multi
	 * @return
	 */
	public String multiplyNumber(Arithmetic multi) {
		String result = "";
		try{
			r = x*y;
			result = "La multiplicacion de " + x + " * " + y + " es igual a: " + r;
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return result;
	}
	/**
	 * 
	 * @param div
	 * @return
	 */

	public String dividingNumber(Arithmetic div) {
		String result = "";
		
		try {
			r = x/y;
			result = "La divisi�n de " + x + " / " + y + " es igual a: " + r;
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return result;
	}
	
}
