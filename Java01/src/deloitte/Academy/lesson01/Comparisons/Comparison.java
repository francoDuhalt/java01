package deloitte.Academy.lesson01.Comparisons;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.Academy.lesson01.run.Ejecutar;

/**
 * 
 * @author Franco Duhalt
 *
 */
public class Comparison {
	private static final Logger LOGGER = Logger.getLogger(Ejecutar.class.getName());
	public int x;
	public int y;
	
	/**
	 * Comparation Class
	 * @return
	 */
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	/**
	 * 
	 * @param equal
	 * @return
	 */
	public String equalNumber(Comparison equal) {
		String mensaje = "";
		
		try {
			if(x == y) {
				mensaje = "Son iguales";
			}else {
				mensaje = "No son iguales";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return mensaje;
	}
	
	/**
	 * 
	 * @param less
	 * @return
	 */
	public String lessNumber(Comparison less) {
		String mensaje = "";
		
		try {
			if(x < y) {
				mensaje = x + " es menor que " + y;
			}else {
				mensaje = "No Aplica";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return mensaje;
	}
	
	/**
	 * 
	 * @param lessEqual
	 * @return
	 */
	public String lessEqualNumber(Comparison lessEqual) {
		String mensaje = "";
		
		try {
			if(x <= y) {
				mensaje = x + " es menor o igual que " + y;
			}else {
				mensaje = "No Aplica";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return mensaje;
	}
	
	/**
	 * 
	 * @param greater
	 * @return
	 */
	
	public String greaterNumber(Comparison greater) {
		String mensaje = "";
		try {
			if(x > y) {
				mensaje = x + " es mayor que " + y;
			}else {
				mensaje = "No Aplica";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return mensaje;
	}
	
	/**
	 * 
	 * @param greaterEqual
	 * @return
	 */
	public String greaterEqualNumber(Comparison greaterEqual) {
		String mensaje = "";
		
		try {
			if(x >= y) {
				mensaje = x + " es mayor o igual que " + y;
			}else {
				mensaje = "No Aplica";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return mensaje;
	}
	
}
