package deloitte.Academy.lesson01.LogicalOperators;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.Academy.lesson01.run.Ejecutar;

/**
 * 
 * @author Franco Duhalt
 *
 */

public class LogicalOperators {
	private static final Logger LOGGER = Logger.getLogger(Ejecutar.class.getName());
	public int x;
	public int y;
	
	/**
	 * Logical Operators Class
	 * @return
	 */
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * 
	 * @param andOperator
	 * @return
	 */
	public String andOperator(LogicalOperators andOperator) {
		String mensaje = "";
		
		try {
			if ((x == 0) && (y==0)) {
				mensaje = "Tienes valores en 0";
			}else {
				mensaje = "No Aplica";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		return mensaje;
	}
	/**
	 * 
	 * @param orOperator
	 * @return
	 */
	public String orOperator(LogicalOperators orOperator) {
		String mensaje = "";
		try {
			if ((x == 0) || (y==0)) {
				mensaje = "Uno de los valores es 0";
			}else {
				mensaje = "No Aplica";
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return mensaje;
	}
	
	
}
